﻿using System.Collections.Generic;
using System.Linq;
using AutoMoFor.DataBase;

namespace AutoMoFor.Commands
{
    class CommandsProcessor
    {
        private List<ICommand> _commands;
        private readonly IAutoMoForDataBase _dataBase;

        public ICommand CommandNotFoundCommand { get; set; }

        public ICommand MoreThanOneCommandMatchesAlias { get; set; }

        public CommandsProcessor(IAutoMoForDataBase dataBase)
        {
            _dataBase = dataBase;
        }

        public void Initialize()
        {
            var processPostCommand = new ProcessPostCommand(_dataBase);
            _commands = new List<ICommand>
            {
                new AddPostCommand(_dataBase),
                new ListCommand(_dataBase), 
                new ExitCommand(), 
                new LoadSmallSampleDataCommand(_dataBase),
                processPostCommand,
                new DownloadAndSerializeForumCommand(),
                new DeSerializeDataCommand(_dataBase),
                new ClearCommand(),
                new ProcessAllPostsCommand(_dataBase, processPostCommand),
                new ShowPostCommand(_dataBase)
            };

            _commands.Add(new HelpCommand(_commands));

            CommandNotFoundCommand = new CommandNotFoundCommand(_commands);
            MoreThanOneCommandMatchesAlias = new VerboseCommand("More than one command matches the given allias");
        }

        public ICommand RecognizeCommand(string input)
        {
            var commandAlias = input.Split(' ').First();
            var recognizedCommands = new List<ICommand>();
            foreach (var command in _commands)
            {
                if(command.Alliases.Contains(commandAlias))
                    recognizedCommands.Add(command);
            }
         
            var recognizedCommandsCount = recognizedCommands.Count;
            
            switch (recognizedCommandsCount)
            {
                case 0:
                    return CommandNotFoundCommand;
                    break;
                case 1:
                    return recognizedCommands.Single();
                    break;
                default:
                    return MoreThanOneCommandMatchesAlias;
                    break;
            }
        }
    }
}