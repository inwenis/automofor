﻿using System;
using System.Collections.Generic;
using AutoMoFor.Commands.Helpers;
using AutoMoFor.DataBase;

namespace AutoMoFor.Commands
{
    internal class ShowPostCommand : ICommand
    {
        private readonly IAutoMoForDataBase _dataBase;

        public ShowPostCommand(IAutoMoForDataBase dataBase)
        {
            _dataBase = dataBase;
        }

        public IReadOnlyCollection<string> Alliases
        {
            get
            {
                return new[]
                {
                    "showpost"
                };
            }
        }

        public string Name
        {
            get { return "ShowPostCommand"; }
        }

        public void Execute(string input)
        {
            var parsedInput = InputParser.ParseInput(input);
            ValidateInput(parsedInput);
            var stringid = parsedInput["postid"];
            var id = Guid.Parse(stringid);
            var post = _dataBase.GetPost(id);
            Console.WriteLine(Printer.GetUserFriendlyString(post));
        }

        private void ValidateInput(IReadOnlyDictionary<string, string> input)
        {
            if (false == input.ContainsKey("postid"))
            {
                throw new Exception("Postid is a mandatory value!");
            }
        }
    }
}