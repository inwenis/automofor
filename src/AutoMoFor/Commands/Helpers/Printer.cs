using AutoMoFor.Models;

namespace AutoMoFor.Commands.Helpers
{
    class Printer
    {
        public static string GetUserFriendlyString(User user)
        {
            return string.Format("\tUser [Name: {0}, Id: {1}]", user.Name, user.Id.ToString("N"));
        }

        public static string GetUserFriendlyString(Post post)
        {
            return string.Format("\n\tPost \n\t\t[Content: \"{0}\", \n\t\tCreationTimeStamp: {1}, \n\t\tId: {2}, \n\t\tqualifier {3}]", post.Content, post.CreationTimeStamp, post.Id.ToString("N"), post.Qualifier);
        }
    }
}