using System;
using System.Collections.Generic;
using System.Linq;
using AutoMoFor.Commands.Helpers;
using AutoMoFor.DataBase;
using AutoMoFor.Models;

namespace AutoMoFor.Commands
{
    class ListCommand : ICommand
    {
        private readonly IAutoMoForDataBase _dataBase;

        public ListCommand(IAutoMoForDataBase dataBase)
        {
            _dataBase = dataBase;
        }

        public virtual IReadOnlyCollection<string> Alliases
        {
            get
            {
                return new[]
                {
                    "list",
                    "lst",
                    "l"
                };
            }
        }

        public virtual string Name
        {
            get { return "List"; }
        }

        public virtual void Execute(string input)
        {
            var parsedInput = InputParser.ParseInput(input);
            if (parsedInput.ContainsKey("recent"))
                PrintRecent(parsedInput);
            else if (parsedInput.ContainsKey("qualified"))
                PrintQualified(parsedInput);
            else
                PrintAll();
        }

        private void PrintQualified(IReadOnlyDictionary<string, string> input)
        {
            var threshhold = GetThreshold(input);
            var postsToPrint =
                _dataBase.Posts.Where(post => post.Qualifier >= threshhold).OrderBy(post => post.Qualifier);
            PrintPosts(postsToPrint);
        }

        private static int GetThreshold(IReadOnlyDictionary<string, string> input)
        {
            if (input.ContainsKey("threshold"))
                return Int32.Parse(input["threshold"]);
            return 5;
        }

        private void PrintRecent(IReadOnlyDictionary<string, string> input)
        {
            var numberOfPostsToDisplay = Int32.Parse(input["number"]);
            var postsToPrint = _dataBase.Posts.OrderByDescending(post => post.AddedToDataBaseTimeStamp).Take(numberOfPostsToDisplay);
            PrintPosts(postsToPrint);
        }

        private void PrintAll()
        {
            PrintPosts(_dataBase.Posts);
            PrintUsers(_dataBase.Users);
        }

        private void PrintUsers(IEnumerable<User> users)
        {
            Console.WriteLine("Users");
            foreach (var user in users)
            {
                Console.WriteLine(Printer.GetUserFriendlyString(user));
            }
        }

        private void PrintPosts(IEnumerable<Post> posts)
        {
            Console.WriteLine("Posts");
            foreach (var post in posts)
            {
                Console.WriteLine(Printer.GetUserFriendlyString(post));
            }
        }
    }
}