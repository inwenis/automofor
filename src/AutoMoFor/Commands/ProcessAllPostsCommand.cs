﻿using System;
using System.Collections.Generic;
using System.Linq;
using AutoMoFor.DataBase;
using AutoMoFor.Models;

namespace AutoMoFor.Commands
{
    internal class ProcessAllPostsCommand : ICommand
    {
        private readonly IAutoMoForDataBase _dataBase;
        private readonly ProcessPostCommand _processPostCommand;

        public ProcessAllPostsCommand(IAutoMoForDataBase dataBase, ProcessPostCommand processPostCommand)
        {
            _dataBase = dataBase;
            _processPostCommand = processPostCommand;
        }

        public IReadOnlyCollection<string> Alliases
        {
            get
            {
                return new[]
                {
                    "processposts",
                    "bigprocess",
                    "processtopicsl",
                    "processall"
                };
            }
        }

        public string Name
        {
            get { return "ProcessAllPostsCommand"; }
        }

        public void Execute(string input)
        {
            var posts = _dataBase.Posts.ToArray();
            Console.WriteLine("There are {0} posts to process, are you sure you want to continue? (y/n)", posts.Count());
            var key = Console.ReadKey();
            if (key.KeyChar != 'y')
                return;
            foreach (var post in posts)
            {
                ProcessPost(post);
            }
        }

        private void ProcessPost(Post post)
        {
            var input = String.Format("processpost -postid {0}", post.Id.ToString("N"));
            _processPostCommand.Execute(input);
        }
    }
}