﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using AutoMoFor.DataBase;
using AutoMoFor.Models;

namespace AutoMoFor.Commands
{
    internal class DeSerializeDataCommand : ICommand
    {
        private readonly IAutoMoForDataBase _dataBase;

        public DeSerializeDataCommand(IAutoMoForDataBase dataBase)
        {
            _dataBase = dataBase;
        }

        public IReadOnlyCollection<string> Alliases
        {
            get
            {
                return new[]
                {
                    "deserialize",
                    "deserializedata",
                    "datafromfile",
                };
            }
        }

        public string Name
        {
            get { return "DeSerialize"; }
        }

        public void Execute(string input)
        {
            var topics = LoadTopics();
            Console.WriteLine("Would you like to save the topics to the data-base? (y/n)");
            var key = Console.ReadKey();
            if (key.KeyChar == 'y')
            {
                foreach (var topic in topics)
                {
                    var id = _dataBase.AddTopic(topic);
                    Console.WriteLine("Topic added with id {0}", id);
                }
            }
        }

        private static List<Topic> LoadTopics()
        {
            Console.WriteLine("Reading topics from file");
            var stream = File.Open("topics.osl", FileMode.Open);
            var bformatter = new BinaryFormatter();
            var topics = (List<Topic>) bformatter.Deserialize(stream);
            stream.Close();
            Console.WriteLine("Done!");
            return topics;
        }
    }
}