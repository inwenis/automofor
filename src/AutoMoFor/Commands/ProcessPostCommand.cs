using System;
using System.Collections.Generic;
using AutoMoFor.Commands.Helpers;
using AutoMoFor.DataBase;
using AutoMoFor.Rules;

namespace AutoMoFor.Commands
{
    internal class ProcessPostCommand : ICommand
    {
        private readonly IAutoMoForDataBase _dataBase;
        private readonly IEnumerable<IRule> _rules;

        public ProcessPostCommand(IAutoMoForDataBase dataBase)
        {
            _dataBase = dataBase;
            _rules = new List<IRule>
            {
                new SwearwordsRule(),
                new EmptyPostsRule(),
                new TimeBetweenPostsRule(),
                new SentimentRule(),
                new SpamerRule(),
            };
        }

        public virtual IReadOnlyCollection<string> Alliases
        {
            get
            {
                return new[]
                {
                    "processpost",
                    "processpostcommand",
                    "process",
                    "getqualifier",
                    "qualify"
                };
            }
        }

        public virtual string Name
        {
            get { return "ProcessPost"; }
        }

        public virtual void Execute(string input)
        {
            var parsedInput = InputParser.ParseInput(input);
            ValidateInput(parsedInput);
            
            var postId = parsedInput["postid"];
            var postGuid = Guid.Parse(postId);
            var post = _dataBase.GetPost(postGuid);

            double totalQualifier = 0;
            foreach (var rule in _rules)
            {
                var qualifier = rule.GetQualifier(post);
                totalQualifier += qualifier;
                if (parsedInput.ContainsKey("verbose"))
                {
                    Console.WriteLine("{0} - qualifier: {1}", rule.GetType(), qualifier);
                }
            }
            Console.WriteLine("Total qualifer: {0}, postid: {1}", totalQualifier, post.Id.ToString("N"));
            post.Qualifier = totalQualifier;
        }

        private void ValidateInput(IReadOnlyDictionary<string, string> intpu)
        {
            if(false == intpu.ContainsKey("postid"))
                throw new Exception("postid is a mandatory argument");
        }
    }
}