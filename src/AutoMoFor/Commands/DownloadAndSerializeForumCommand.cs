﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using AutoMoFor.Commands.Helpers;
using AutoMoFor.Models;

namespace AutoMoFor.Commands
{
    internal class DownloadAndSerializeForumCommand : ICommand
    {
        public IReadOnlyCollection<string> Alliases
        {
            get
            {
                return new[]
                {
                    "downloadforum",
                    "sampledata",
                    "bigsampledata",
                    "downloadtopics"
                };
            }
        }

        public string Name
        {
            get
            {
                return "DownloadAndSerializeForum";
            }
        }

        public void Execute(string input)
        {
            Console.WriteLine("This may tak a while do you want to continue? (y/n)");
            var key = Console.ReadKey();
            if (key.KeyChar == 'y')
            {
                DownloadForum(input);
            }
            else
            {
                Console.WriteLine("Will now exit download command");
            }
        }

        private void DownloadForum(string input)
        {
            var parsedInput = InputParser.ParseInput(input);

            var downloader = new ThavisaForumDownloader();
            List<Topic> topics;
            if (parsedInput.ContainsKey("onlyone"))
                topics = downloader.DownloadOneTopic();
            else
                topics = downloader.DownloadAllTopics();

            SerializeTopicsToFile(topics);
        }

        private static void SerializeTopicsToFile(List<Topic> topics)
        {
            Console.WriteLine("Will now serialize topics");
            var stream = File.Open("topics.osl", FileMode.Create);
            var bformatter = new BinaryFormatter();
            bformatter.Serialize(stream, topics);
            stream.Close();
            Console.WriteLine("Done!");
        }
    }
}