using System;
using System.Collections.Generic;
using AutoMoFor.Commands.Helpers;
using AutoMoFor.DataBase;
using AutoMoFor.Models;

namespace AutoMoFor.Commands
{
    class AddPostCommand : ICommand
    {
        private readonly IAutoMoForDataBase _dataBase;

        public AddPostCommand(IAutoMoForDataBase dataBase)
        {
            _dataBase = dataBase;
        }

        public virtual IReadOnlyCollection<string> Alliases
        {
            get
            {
                return new[]
                {
                    "addpost",
                    "addpostcommand",
                    "ap",
                    "newpost",
                    "newpostcommand",
                };
            }
        }

        public virtual string Name
        {
            get { return "AddPost"; }
        }

        public virtual void Execute(string input)
        {
            var parsedInput = InputParser.ParseInput(input);
            if (false == ValidateInput(parsedInput))
                return;
            var text = parsedInput["content"];
            var user = GetUser(parsedInput);
            var creationTimeStamp = GetDate(parsedInput);
            var newPost = new Post
            {
                Content = text,
                User = user,
                CreationTimeStamp = creationTimeStamp
            };
            var postId = _dataBase.AddPost(newPost);
            Console.WriteLine("Added post with id {0}", postId.ToString("N"));
        }

        private bool ValidateInput(IReadOnlyDictionary<string, string> options)
        {
            if (false == options.ContainsKey("content"))
            {
                Console.WriteLine("Can not add a post with out content");
                return false;
            }
            return true;
        }

        private User GetUser(IReadOnlyDictionary<string, string> input)
        {
            if(input.ContainsKey("user"))
            {
                var stringId = input["user"];
                var id = Guid.Parse(stringId);
                return _dataBase.GetUser(id);
            }
            return new User();
        }

        private static DateTime GetDate(IReadOnlyDictionary<string, string> input)
        {
            if (input.ContainsKey("date"))
            {
                return DateTime.Parse(input["date"]);
            }
            else
            {
                return DateTime.Now;
            }
        }
    }
}