using System;
using System.Collections.Generic;
using AutoMoFor.DataBase;
using AutoMoFor.Models;

namespace AutoMoFor.Commands
{
    class LoadSmallSampleDataCommand : ICommand
    {
        private readonly IAutoMoForDataBase _dataBase;

        public LoadSmallSampleDataCommand(IAutoMoForDataBase dataBase)
        {
            _dataBase = dataBase;
        }

        public virtual IReadOnlyCollection<string> Alliases
        {
            get
            {
                return new[]
                {
                    "loadsmallsample",
                    "smallsample",
                    "smallsampledata"
                };
            }
        }

        public virtual string Name
        {
            get { return "LoadSmallSampleData"; }
        }

        public virtual void Execute(string input)
        {
            var user = new User
            {
                Name = "qwerty"
            };

            var fwordPost = new Post
            {
                CreationTimeStamp = DateTime.Now, User = user, Content = "I am mother f69ucker\n dude"
            };
            var emptyPost = new Post
            {
                CreationTimeStamp = DateTime.Now + TimeSpan.FromMinutes(12), User = user
            };
            var earlyPost = new Post
            {
                CreationTimeStamp = DateTime.Now + TimeSpan.FromMinutes(12) + TimeSpan.FromSeconds(56), User = user
            };
            var haterPost = new Post
            {
                Content = "I hate the whole world, i would like to kill all, and fuck all the sexy people, and kill rock and roll", User = user
            };
            var happyPost = new Post
            {
                Content = "I am so happy", User = user
            };

            user.Posts = new List<Post>(new[] { fwordPost, emptyPost, earlyPost, happyPost, haterPost });

            _dataBase.AddUser(user);
            foreach (var post in user.Posts)
            {
                _dataBase.AddPost(post);
            }
        }
    }
}