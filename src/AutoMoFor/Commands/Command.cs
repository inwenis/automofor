﻿using System.Collections.Generic;

namespace AutoMoFor.Commands
{
    internal interface ICommand
    {
        IReadOnlyCollection<string> Alliases { get; }
        string Name { get; }
        void Execute(string input);
    }
}