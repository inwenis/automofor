﻿using System;
using System.Collections.Generic;
using System.Linq;
using AutoMoFor.Models;

namespace AutoMoFor.DataBase
{
    class InMemoryDataBase : IAutoMoForDataBase
    {
        private readonly IList<User> _users;
        private readonly IList<Post> _posts;
        private readonly IList<Topic> _topics;

        public IEnumerable<User> Users
        {
            get { return _users; }
        }

        public IEnumerable<Post> Posts
        {
            get { return _posts; }
        }

        public IEnumerable<Topic> Topics
        {
            get { return _topics; }
        }

        public InMemoryDataBase()
        {
            _users = new List<User>();
            _posts = new List<Post>();
            _topics = new List<Topic>();
        }

        public User GetUser(Guid id)
        {
            var selectedUser = _users.SingleOrDefault(user => user.Id == id);
            if( selectedUser == null)
            {
                throw new Exception(String.Format("A user with id {0} does not exist", id.ToString("N")));
            }
            return selectedUser;
        }

        public Post GetPost(Guid id)
        {
            return _posts.Single(post => post.Id == id);
        }

        public Guid AddUser(User user)
        {
            SetIdAndDatetime(user);
            user.Id = Guid.NewGuid();
            _users.Add(user);
            return user.Id;
        }

        public Guid AddPost(Post post)
        {
            SetIdAndDatetime(post);
            _posts.Add(post);
            return post.Id;
        }

        public Guid AddTopic(Topic topic)
        {
            SetIdAndDatetime(topic);
            _topics.Add(topic);

            foreach (var post in topic.Posts)
            {
                AddPost(post);
            }
            return topic.Id;
        }

        private static void SetIdAndDatetime(DataBaseModel post)
        {
            post.Id = Guid.NewGuid();
            post.AddedToDataBaseTimeStamp = DateTime.Now;
        }
    }
}