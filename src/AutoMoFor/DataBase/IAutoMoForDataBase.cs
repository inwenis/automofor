﻿using System;
using System.Collections.Generic;
using AutoMoFor.Models;

namespace AutoMoFor.DataBase
{
    internal interface IAutoMoForDataBase
    {
        IEnumerable<User> Users { get; }
        IEnumerable<Post> Posts { get; }
        IEnumerable<Topic> Topics { get; }

        User GetUser(Guid id);
        Post GetPost(Guid id);

        Guid AddUser(User user);
        Guid AddPost(Post post);
        Guid AddTopic(Topic topic);
    }
}