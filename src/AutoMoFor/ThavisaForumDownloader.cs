﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using AutoMoFor.Models;
using HtmlAgilityPack;

namespace AutoMoFor
{
    class ThavisaForumDownloader
    {
        private const string ForumUrl = "http://www.thaivisa.com/forum/forum/2-general-topics/";

        public List<Topic> DownloadAllTopics()
        {
            try
            {
                var htmlDocument = new HtmlDocument();
                htmlDocument.Load(GetStreamFromUrl(ForumUrl));
                var topicLinks = htmlDocument.DocumentNode.Descendants("a").Where(IsTopicLink()).ToArray();
                return DownloadTopics(topicLinks);
            }
            catch
            {
                throw new Exception("Something went wrong while making the request to the Thavisa forum.");
            }
        }

        public List<Topic> DownloadOneTopic()
        {
            try
            {
                var htmlDocument = new HtmlDocument();
                htmlDocument.Load(GetStreamFromUrl(ForumUrl));
                var topicLinks = htmlDocument.DocumentNode.Descendants("a").Where(IsTopicLink()).Take(1).ToArray();
                return DownloadTopics(topicLinks);
            }
            catch
            {
                throw new Exception("Something went wrong while making the request to the Thavisa forum.");
            }
        }

        private List<Topic> DownloadTopics(HtmlNode[] topicLinks)
        {
            var topics = new List<Topic>();
            Console.WriteLine("Will now download {0} topics", topicLinks.Count());
            var counter = 1;
            foreach (var topicLink in topicLinks)
            {
                Console.WriteLine("Downloading {0} topic...", counter);
                var name = topicLink.SelectSingleNode("span").InnerText;
                var url = topicLink.Attributes["href"].Value;
                var posts = DownloadPosts(url);
                var topic = new Topic
                {
                    Name = name,
                    Link = url,
                    Posts = posts
                };
                topics.Add(topic);
                ++counter;
            }
            Console.WriteLine("Done downloading topics!");
            return topics;
        }

        private static Func<HtmlNode, bool> IsTopicLink()
        {
            return n => n.Attributes.Contains("class") && n.Attributes["class"].Value.Contains("topic_title");
        }

        List<Post> DownloadPosts(string url)
        {
            var posts = new List<Post>();
            var htmlDocument = new HtmlDocument();
            htmlDocument.Load(GetStreamFromUrl(url));
            var postsNodes = htmlDocument.DocumentNode.Descendants("div").Where(IsPost());
            foreach (var post in postsNodes)
            {
                var postsContentNodes = post.Descendants("div").Where(IsPostContent()).ToArray();
                var postsDatesNodes = post.Descendants("abbr").Where(IsDateNode()).ToArray();
                if (postsContentNodes.Any())
                {
                    var content = postsContentNodes.First().InnerText;
                    var date = new DateTime();
                    try
                    {
                        date = DateTime.Parse(postsDatesNodes.First().InnerText);
                    }
                    catch (FormatException) { }
                    posts.Add(new Post
                    {
                        Content = content,
                        CreationTimeStamp = date
                    });
                }
            }
            return posts;
        }

        private static Func<HtmlNode, bool> IsDateNode()
        {
            return n => n.Attributes.Contains("class") && n.Attributes["class"].Value.Contains("published");
        }

        private static Func<HtmlNode, bool> IsPostContent()
        {
            return n => n.Attributes.Contains("class") && n.Attributes["class"].Value.Contains("post")
                        && n.Attributes["class"].Value.Contains("entry-content");
        }

        private static Func<HtmlNode, bool> IsPost()
        {
            return n => n.Attributes.Contains("class") && n.Attributes["class"].Value.Contains("post_block");
        }


        private StreamReader GetStreamFromUrl(string url)
        {
            var request = (HttpWebRequest)WebRequest.Create(url);
            var response = (HttpWebResponse)request.GetResponse();
            var receiveStream = response.GetResponseStream();
            var encoding = response.CharacterSet == null ? Encoding.Default : Encoding.GetEncoding(response.CharacterSet);
            var readStream = new StreamReader(receiveStream, encoding);
            return readStream;
        }
    }
}
