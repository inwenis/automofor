﻿using System;
using AutoMoFor.Commands;
using AutoMoFor.DataBase;

//todo add users to extract from posts

namespace AutoMoFor
{
    class Program
    {
        public static bool Exit;
        private static CommandsProcessor _commandsProcessor;

        static void Main()
        {
            Initialize();

            while (Exit == false)
            {
                var input = Console.ReadLine();
                var command = _commandsProcessor.RecognizeCommand(input);
                try
                {
                    command.Execute(input);
                }
                catch (Exception e)
                {
                    HandleException(e);
                }
            }
        }

        private static void HandleException(Exception exception)
        {
            Console.ForegroundColor = ConsoleColor.Red;
            var message = string.Format("The command has thrown an exception: {0}", exception.Message);
            Console.WriteLine(message);
            Console.ForegroundColor = ConsoleColor.Gray;
        }

        private static void Initialize()
        {
            var dataBase = new InMemoryDataBase();
            _commandsProcessor = new CommandsProcessor(dataBase);
            _commandsProcessor.Initialize();
        }
    }
}