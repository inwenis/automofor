﻿using System;
using System.Linq;
using AutoMoFor.Models;

namespace AutoMoFor.Rules
{
    class TimeBetweenPostsRule : IRule
    {
        private static readonly TimeSpan MinimumTimeSpan = new TimeSpan(0,0,0,60);

        public double GetQualifier(Post post)
        {
            var user = post.User;
            if (user.Posts.Count < 2)
                return 0;
            var orderedPosts = user.Posts.OrderByDescending(p => p.CreationTimeStamp);
            var previousPost = orderedPosts.ElementAt(1);

            var timeSpan = post.CreationTimeStamp - previousPost.CreationTimeStamp;
            if (timeSpan > MinimumTimeSpan)
                return 0;

            var a = (MinimumTimeSpan.TotalSeconds - timeSpan.TotalSeconds) / MinimumTimeSpan.TotalSeconds;
            return a;
        }
    }
}