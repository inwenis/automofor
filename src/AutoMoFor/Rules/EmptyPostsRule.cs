﻿using System;
using AutoMoFor.Models;

namespace AutoMoFor.Rules
{
    public class EmptyPostsRule : IRule
    {
        public double GetQualifier(Post post)
        {
            if (String.IsNullOrWhiteSpace(post.Content))
                return 1.0;
            return 0.0;
        }
    }
}