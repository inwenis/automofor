﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Text;
using System.Web;
using System.Web.Script.Serialization;
using AutoMoFor.Models;

namespace AutoMoFor.Rules
{
    public class SpamerRule : IRule
    {
        public double GetQualifier(Post post)
        {
            var user = post.User;
            Dictionary<string, object> resposne;
            try
            {
                resposne = MakeWebRequest(user.Name);
            }
            catch (WebException e) //propably no internet connection
            {
                return 0.0;
            }

            if (false == resposne.ContainsKey("success"))
                return 0.0;

            var spamValue = 0.0;
            spamValue += GetQualifierFor(resposne, "username");
            spamValue += GetQualifierFor(resposne, "ip");
            spamValue += GetQualifierFor(resposne, "email");

            if (spamValue > 100.0)
                return 1.0;

            return spamValue / 100.0;
        }

        private double GetQualifierFor(IReadOnlyDictionary<string, object> response, string key)
        {
            double result = 0;
            if (response.ContainsKey(key))
            {
                var dictionary = (Dictionary<string, object>) response[key];
                if (((int) dictionary["appears"]) != 0)
                {
                    result += 10.0;
                    object val;
                    if (dictionary.TryGetValue("confidence", out val))
                        result += decimal.ToDouble((decimal) val);
                }
            }
            return result;
        }

        private static Dictionary<string, object> MakeWebRequest(string userName)
        {
            var url = new StringBuilder("http://www.stopforumspam.com/api?");
            if (false == string.IsNullOrEmpty(userName))
                url.AppendFormat("username={0}&", HttpUtility.UrlEncode(userName));

            // Needs change in model

            //if (!string.IsNullOrEmpty(user.Ip))
            //    url.AppendFormat("ip={0}&", HttpUtility.UrlEncode(user.Ip));

            //if (!string.IsNullOrEmpty(user.Email))
            //    url.AppendFormat("email={0}&", HttpUtility.UrlEncode(user.Email));

            url.Append("f=json");

            var request = (HttpWebRequest) WebRequest.Create(new Uri(url.ToString()));
            request.Method = "GET";
            request.Timeout = 5000;
            request.UserAgent = "AutoMoFor";

            var response = request.GetResponse();
            string output;
            using (var readStream = response.GetResponseStream())
            {
                using (var responseStream = new StreamReader(readStream, Encoding.UTF8))
                {
                    output = responseStream.ReadToEnd();
                }
            }

            var deserializer = new JavaScriptSerializer();
            var result = (Dictionary<string, object>) deserializer.DeserializeObject(output);
            return result;
        }
    }
}