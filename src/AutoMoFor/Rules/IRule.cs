﻿using AutoMoFor.Models;

namespace AutoMoFor.Rules
{
    public interface IRule
    {
        double GetQualifier(Post post);
    }
}