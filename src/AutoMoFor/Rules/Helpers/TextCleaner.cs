﻿using System.Linq;
using System.Text.RegularExpressions;

namespace AutoMoFor.Rules.Helpers
{
    class TextCleaner
    {
        public string Clean(string text)
        {
            var textWithoutInvalidCharacters = RemoveInvalidCharacters(text);
            var textWithoutMultispaces = RemoveMultiSpaces(textWithoutInvalidCharacters);
            return textWithoutMultispaces;
        }

        private static string RemoveMultiSpaces(string text)
        {
            var textWithoutMultispaces = Regex.Replace(text, @"\W{2,}", "");
            return textWithoutMultispaces;
        }

        private static string RemoveInvalidCharacters(string post)
        {
            var invalidSymbols = new[]
            {
                '`', '!', '@', '#', '$', '%', '^', '&', '*', '(', ')', '1', '2', '3', '4', '5', '6',
                '7', '8', '9', '0', '-', '_', '+', '=', '{', '[', '}', ']', '\\', '|', ';', ':', '"', '\'', ',', '<', '.', '>',
                '?', '/', '\n', '\r'
            };

            string textWithoutInvalidSymbols = "";
            foreach (var letter in post)
            {
                if (false == invalidSymbols.Contains(letter))
                    textWithoutInvalidSymbols += letter;
            }
            return textWithoutInvalidSymbols;
        }
    }
}