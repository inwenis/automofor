﻿using System.Collections.Generic;
using System.IO;
using AutoMoFor.Models;
using AutoMoFor.Rules.Helpers;

namespace AutoMoFor.Rules
{
    public class SwearwordsRule : IRule
    {
        private readonly TextCleaner _textCleaner;

        private readonly string[] _swearWords;

        public SwearwordsRule()
        {
            _textCleaner = new TextCleaner();
            _swearWords = File.ReadAllLines("badwords.txt");
        }

        public double GetQualifier(Post post)
        {
            var cleanupText = _textCleaner.Clean(post.Content);
            var value = Profanity(cleanupText);
            return value;
        }

        private double Profanity(string text)
        {
            var wordDelimiters = new[] { ' ', '\n' };
            text = text.ToLower();
            var swearWordsFoundCount = CountSwearWords(text, _swearWords);
            var wordCount = text.Split(wordDelimiters).Length;
            double value = (double) swearWordsFoundCount / wordCount;
            return value;
        }

        private int CountSwearWords(string text, IEnumerable<string> swearWords)
        {
            int swearWordsFoundCount = 0;
            foreach (var swearWord in swearWords)
            {
                if (text.Contains(swearWord))
                {
                    ++swearWordsFoundCount;
                }
            }
            return swearWordsFoundCount;
        }
    }
}