﻿using System;
using System.IO;
using System.Linq;
using System.Net;
using System.Text.RegularExpressions;
using System.Web;
using AutoMoFor.Models;

namespace AutoMoFor.Rules
{
    public class SentimentRule : IRule
    {
        const double DefaultQualifier = 0;

        public double GetQualifier(Post post)
        {
            try
            {
                double score = DefaultQualifier;
                using (var client = new WebClient())
                {
                    var url = GetRequestUrl(post);
                    client.Headers.Add("X-Mashape-Authorization", "KUnx2XLV8g8SvXsT0ITF958oh6LcpMT6");
                    var data = client.OpenRead(url);
                    var reader = new StreamReader(data);
                    const string pattern = @"""sentiment-score\"":\W(.*),";
                    var response = reader.ReadToEnd();
                    var match = Regex.Match(response, pattern);
                    if (match.Success)
                    {
                        var scoreString = match.Groups[1].Value;
                        scoreString = scoreString.Replace('.', ',');
                        score = double.Parse(scoreString);
                    }
                    data.Close();
                    reader.Close();
                    return 0.5 - score;
                }
            }
            catch (WebException e) // propably no internet connection
            {
                return DefaultQualifier;
            }
        }

        private static string GetRequestUrl(Post post)
        {
            var encodedContent = HttpUtility.UrlEncode(post.Content);
            var url = "https://loudelement-free-natural-language-processing-service.p.mashape.com/nlp-text/?text=" +
                      encodedContent;
            return url;
        }
    }
}