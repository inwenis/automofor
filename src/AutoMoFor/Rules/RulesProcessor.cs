﻿using AutoMoFor.Models;

namespace AutoMoFor.Rules
{
    class RulesProcessor
    {
        private IRule[] rules;

        public void RegisterRules()
        {
            rules = new IRule[]
            {
                new SwearwordsRule(),
                new EmptyPostsRule(),
                new TimeBetweenPostsRule(),
                new SentimentRule(),
                new SpamerRule()
            };
        }

        public double ProcessPost(Post post)
        {
            double qualifier = 0;
            foreach (var rule in rules)
            {
                qualifier += rule.GetQualifier(post);
            }
            return qualifier;
        }
    }
}