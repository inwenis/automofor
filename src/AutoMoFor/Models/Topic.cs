﻿using System;
using System.Collections.Generic;

namespace AutoMoFor.Models
{
    [Serializable]
    class Topic : DataBaseModel
    {
        public string Name;
        public string Link;
        public List<Post> Posts;

        public Topic()
        {
            Posts = new List<Post>();
        }
    }
}
