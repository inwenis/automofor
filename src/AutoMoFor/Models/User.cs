﻿using System;
using System.Collections.Generic;

namespace AutoMoFor.Models
{
    [Serializable]
    public class User : DataBaseModel
    {
        public User()
        {
            Posts = new List<Post>();
        }

        public List<Post> Posts;
        public string Name;
    }
}