﻿using System;

namespace AutoMoFor.Models
{
    [Serializable]
    public class DataBaseModel
    {
        public Guid Id;
        public DateTime AddedToDataBaseTimeStamp;
    }
}