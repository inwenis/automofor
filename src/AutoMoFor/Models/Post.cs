﻿using System;

namespace AutoMoFor.Models
{
    [Serializable]
    public class Post : DataBaseModel
    {
        public Post()
        {
            CreationTimeStamp = DateTime.Now;
            Content = "";
            User = new User();
        }

        public DateTime CreationTimeStamp; 
        public string Content;
        public User User;
        public double Qualifier;

        public override string ToString()
        {
            return Content;
        }
    }
}