﻿using AutoMoFor.Models;
using AutoMoFor.Rules;
using Machine.Specifications;

namespace AutoMoForTests
{
    [Subject(typeof(SentimentRule))]
    internal class given_sentiment_rule
    {
        Establish context = () =>
        {
            sut = new SentimentRule();
            post = new Post();
        };

        Because of = () => result = sut.GetQualifier(post);

        protected static SentimentRule sut;
        protected static double result;
        protected static Post post;
    }

    [Subject(typeof(SentimentRule))]
    class when_post_content_is_negative : given_sentiment_rule
    {
        Establish context = () => post.Content = "I hate the whole world, i would like to kill all, and fuck all the sexy people, and kill rock and roll";
        It should_return_high_value = () => result.ShouldBeGreaterThan(0.5);
    }

    [Subject(typeof(SentimentRule))]
    class when_post_content_is_positive : given_sentiment_rule
    {
        Establish context = () => post.Content = "i am really happy about that";
        It should_return_low_value = () => result.ShouldBeLessThan(0.2);
    }
}
