﻿using AutoMoFor.Models;
using AutoMoFor.Rules;
using Machine.Specifications;

namespace AutoMoForTests
{
    [Subject(typeof(SpamerRule))]
    internal class given_spamer_rule
    {
        Establish context = () =>
        {
            sut = new SpamerRule();
            post = new Post();
        };

        Because of = () => result = sut.GetQualifier(post);

        protected static SpamerRule sut;
        protected static double result;
        protected static Post post;
    }

    [Subject(typeof(SpamerRule))]
    class when_post_author_is_known_spamer : given_spamer_rule
    {
        Establish context = () =>
            post.User = new User{ Name = "swzmtd2374"};
        It should_return_high_qualifier = () => result.ShouldBeGreaterThan(0);
    }

    [Subject(typeof(SpamerRule))]
    class when_author_is_usual_user : given_spamer_rule
    {
        Establish context = () => post.User = new User{ Name = "marvolo"};
        It should_return_low_value = () => result.ShouldBeLessThan(0.2);
    }
}
