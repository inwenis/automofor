﻿using AutoMoFor.Models;
using AutoMoFor.Rules;
using Machine.Specifications;

namespace AutoMoForTests
{
    [Subject(typeof(SwearwordsRule))]
    internal class given_swearword_rule
    {
        Establish context = () =>
        {
            sut = new SwearwordsRule();
            post = new Post();
        };

        Because of = () => result = sut.GetQualifier(post);

        protected static SwearwordsRule sut;
        protected static double result;
        protected static Post post;
    }

    [Subject(typeof(SwearwordsRule))]
    class when_given_text_without_swearwords : given_swearword_rule
    {
        Establish context = () => post.Content = "hello";
        It should_return_0 = () => result.ShouldEqual(0);
    }

    [Subject(typeof(SwearwordsRule))]
    class when_given_text_with_swearwords : given_swearword_rule
    {
        Establish context = () => post.Content = "hello motherfucker";
        It should_return_value_greater_than_0 = () => result.ShouldBeGreaterThan(0);
    }

    [Subject(typeof(SwearwordsRule))]
    class when_given_text_with_swearword_with_numbers: given_swearword_rule
    {
        Establish context = () => post.Content = "hello fu5656ck";
        It should_return_value_greater_than_0 = () => result.ShouldBeGreaterThan(0);
    }

    [Subject(typeof(SwearwordsRule))]
    class when_given_text_with_swearword_separated_with_whitespaces : given_swearword_rule
    {
        Establish context = () => post.Content = "hello fu  6ck";
        It should_return_value_greater_than_0 = () => result.ShouldBeGreaterThan(0);
    }
}
